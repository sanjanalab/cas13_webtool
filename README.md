# Cas13 Webtool (Supplementary code and analyses)

This repository is intended to accompany our manuscript. For more information, please refer to our *Biorxiv* preprint:

**Transcriptome-wide Cas13 guide RNA design for model organisms and viral RNA pathogens (2020)**

*Xinyi Guo, Hans-Hermann Wessels, Alejandro Méndez-Mancilla, Daniel Haro, Neville E. Sanjana*

The data and code are released to reproduce figures. The optimized Cas13 guides for all reference transcriptomes or RNA genomes analyzed in the manuscript are available on our website (Besides, a web-based tool for customed gRNAs is also available there):

https://cas13design.nygenome.org

For questions about the website and user guide, please see 'website_user_guide_and_FAQ_page.pdf'.

Figures were produced with R v3.5.1. Scripts were labeled separately with figure names (i.e. fig1, fig2a, etc.). The scripts should be able to run on the precomputed data provided in the 'Data' folder to generate the figures. Required libraries were listed at the beginning of each script. For obtaining the desired output, please install all of the additional R library dependencies and download the entire Cas13 webtool directory (includes ‘data’ directory, where we stored relevant raw/pre-computed data for analyses and plotting). We recommend using Rstudio for the analyses, and please set your working directory to be the directory of the folder ‘Cas13_webtool-master’ for the correct loading of any pre-processed data. We provided the example output figures as a reference in the ‘Figures’ directory, please note that running the R scripts will not write the figures to disk. You will be able to visualize it in Rstudio directly, but will need to export the figures if you’d like to have the figure files.

Please raise a Gitlab issue if you find crucial files missing, and we will help out.

If you find our code or precomputed Cas13 guide RNA predictions to be helpful for your work, please consider cite the paper above.

