#!/usr/bin/env Rscript

###################################################################### 
#fig2a: World map of analyzed SARS-CoV-2 isolates (data from GISAID, April 17th, 2020)

setwd('~/Cas13_webtool_code/')

# map the geographical distribution of SARS-CoV-2 genomes used
# load libraries: R version v3.5.2
library(tidyverse)
library(ggplot2)

# extract region/country name
df.loc = read.table('./data/SARS_CoV_2_file_name.txt',
                    stringsAsFactors = F, header = F)
colnames(df.loc) = 'full_name'
df.loc$inter = substring(df.loc$full_name, 9)
df.loc$country = sub("\\_.*", "", df.loc$inter)
df.freq = as.data.frame(table(df.loc$country))
View(df.freq)

# hand-check frequency table, update region/country name, remove non-human data, remove data with incomplete region info
View(subset(df.loc, country == 'bat' |
              country == 'Hong.fa' |
              country == 'New.fa' |
              country == 'South.fa' |
              country == 'Jian' |
              country == 'Tianmen' |
              country == 'pangolin'))

#remove : hCoV_19_canine_Hong.fa | hCoV_19_New.fa | hCoV_19_South.fa
#combine cities in china into a single entry, combine countries in UK into a single entry
#read in updated table
df.freq.clean = read.csv('./data/SARS_CoV_2_region_frequency.csv', header = F,
                         stringsAsFactors = F)
colnames(df.freq.clean) = c('country','genome_count')
head(df.freq.clean)

#install.packages('mapproj')
library('maps')
map.world = map_data('world')
head(map.world)
View(as.data.frame(table(map.world$region)))

map.freq.clean = left_join( map.world, df.freq.clean, by = c('region' = 'country')) 
head(map.freq.clean)

#plot geographical distributions
head(map.freq.clean)
summary(map.freq.clean$genome_count)
#hist(map.freq.clean$genome_count)
#set scale: 0, 1-10, 10-50, 50-100, 100-200, 200-400, 400-800
map.freq.clean$category = findInterval(map.freq.clean$genome_count,c(0.5,10,50,100,200,400,800))
map.freq.clean$category = as.factor(map.freq.clean$category)
ggplot(map.freq.clean, aes( x = long, y = lat, group = group )) +
  geom_polygon(aes(fill = category)) + 
  labs(title = "Design Cas13 crRNAs for SARS-CoV-2 viruses",
       subtitle = "a world map of sequenced SARS-CoV-2 isolates we used",
       caption = "Source: GISAID, April 17 2020",
       fill = "number of genomes sequenced") + 
  scale_fill_manual(values = c("gray90","#f0f9e8","#ccebc5","#a8ddb5","#7bccc4","#43a2ca","#0868ac"),
                    labels = c("0", "1-10", "10-50","50-100","100-200","200-400","400-800")) +
  theme_classic() +
  theme(line = element_blank())
