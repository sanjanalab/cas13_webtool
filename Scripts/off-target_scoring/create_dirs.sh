#!/bin/bash
mkdir bash_script_build_indices/ 
mkdir bash_script_generating_fastas/
mkdir bash_script_generating_final/ 
mkdir bash_script_run_bowtie/
mkdir logs/
mkdir logs/build_indices_logs/  
mkdir logs/generate_fasta_logs/  
mkdir logs/make_final_tables_logs/  
mkdir logs/run_bowtie_logs/
mkdir builds/
mkdir indices/
mkdir fastas/
mkdir bowtie_output/ 
mkdir final_tables/