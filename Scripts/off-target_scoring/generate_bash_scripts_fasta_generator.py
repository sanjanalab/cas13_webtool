import glob
import os
import sys

new_bash_file_name = sys.argv[1].split("/")[-3].lower() + "_" + sys.argv[1].split("/")[-2].lower()

new_bash_file_path = "%s/bash_script_generating_fastas/%s.sh" % (os.getcwd(), new_bash_file_name)
open_new_bash_file = open(new_bash_file_path, "w+")
text = ["#!/bin/bash","#", "#SBATCH --mail-user=user@email.com", "#SBATCH --mail-type=END"]
job_name = "#SBATCH --job-name=%s" % (new_bash_file_name)
output_log = "#SBATCH --output=%s/logs/generate_fasta_logs/%s-%%j.out" % (os.getcwd(), new_bash_file_name)
error_log = "#SBATCH --error=%s/logs/generate_fasta_logs/%s-%%j.err" % (os.getcwd(), new_bash_file_name)
time = "#SBATCH --time=60:00:00"
nodes = "#SBATCH --nodes=1"
tasks_per_node = "#SBATCH --tasks-per-node=1"
cpus_per_task = "#SBATCH --cpus-per-task=20"
memory = "#SBATCH --mem=30G"
empty_line_break_1 = ""
line_break = "###-----Statements to be Executed-----###"
empty_line_break_2 = ""
line_1 = "module purge"
line_2 = "cd %s" % (os.getcwd())
line_3 = "python fasta_generator.py %s %s %s" % (sys.argv[1], sys.argv[2], sys.argv[3])
line_4 = " "
text.extend([job_name,output_log,error_log,time,nodes,tasks_per_node,cpus_per_task,memory,empty_line_break_1,line_break,empty_line_break_2,line_1,line_2,line_3,line_4])
to_write = "\n".join(text)
open_new_bash_file.write(to_write)
open_new_bash_file.close()
