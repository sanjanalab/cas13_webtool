import glob
import os
import sys

folders = glob.glob(sys.argv[1])

for i in folders:
	new_bash_file_name = sys.argv[1].split("/")[-3].lower() + "_" + sys.argv[1].split("/")[-2].lower() + "_" +  i.split("/")[-1].lower()
	new_bash_file_path = "%s/bash_script_generating_final/%s.sh" % (os.getcwd(), new_bash_file_name)
	first_argument = "'" + i + "'"
	second_argument = "'" + sys.argv[2] + i.split("/")[-1] + "'"
	open_new_bash_file = open(new_bash_file_path, "w+")
	text = ["#!/bin/bash","#", "#SBATCH --mail-user=user@email.com", "#SBATCH --mail-type=END"]
	job_name = "#SBATCH --job-name=%s" % (new_bash_file_name)
	output_log = "#SBATCH --output=%s/logs/make_final_tables_logs/%s-%%j.out" % (os.getcwd(), new_bash_file_name)
	error_log = "#SBATCH --error=%s/logs/make_final_tables_logs/%s-%%j.err" % (os.getcwd(), new_bash_file_name)
	time = "#SBATCH --time=40:00:00"
	nodes = "#SBATCH --nodes=1"
	tasks_per_node = "#SBATCH --tasks-per-node=1"
	cpus_per_task = "#SBATCH --cpus-per-task=20"
	memory = "#SBATCH --mem=40G"
	empty_line_break_1 = ""
	line_break = "###-----Statements to be Executed-----###"
	empty_line_break_2 = ""
	line_1 = "module purge"
	line_2 = "cd %s" % (os.getcwd())
	line_3 = "python final_generator.py %s %s %s %s" % (first_argument, second_argument, sys.argv[3], sys.argv[4])
	line_4 = " "
	text.extend([job_name,output_log,error_log,time,nodes,tasks_per_node,cpus_per_task,memory,empty_line_break_1,line_break,empty_line_break_2,line_1,line_2,line_3,line_4])
	to_write = "\n".join(text)
	open_new_bash_file.write(to_write)
	open_new_bash_file.close()



