import glob
import os
import sys

new_bash_file_path = "%s/bash_script_build_indices/%s.sh" % (os.getcwd(), sys.argv[1].split("/")[-1].split(".")[0])
open_new_bash_file = open(new_bash_file_path, "w+")
text = ["#!/bin/bash","#", "#SBATCH --mail-user=user@email.com", "#SBATCH --mail-type=END"]
job_name = "#SBATCH --job-name=%s" % (sys.argv[1].split("/")[-1].split(".")[0])
output_log = "#SBATCH --output=%s/logs/build_indices_logs/%s-%%j.out" % (os.getcwd(), sys.argv[1].split("/")[-1].split(".")[0])
error_log = "#SBATCH --error=%s/logs/build_indices_logs/%s-%%j.err" % (os.getcwd(), sys.argv[1].split("/")[-1].split(".")[0])
time = "#SBATCH --time=60:00:00"
nodes = "#SBATCH --nodes=1"
tasks_per_node = "#SBATCH --tasks-per-node=1"
cpus_per_task = "#SBATCH --cpus-per-task=20"
memory = "#SBATCH --mem=40GB"
empty_line_break_1 = ""
line_break = "###-----Statements to be Executed-----###"
empty_line_break_2 = ""
line_1 = "module purge"
line_2 = "cd %s" % (os.getcwd())
line_3 = "module load bowtie/1.1.2"
line_4 = "python index_builder.py %s %s" % (sys.argv[1], sys.argv[2])
line_5 = " "
text.extend([job_name,output_log,error_log,time,nodes,tasks_per_node,cpus_per_task,memory,empty_line_break_1,line_break,empty_line_break_2,line_1,line_2,line_3,line_4, line_5])
to_write = "\n".join(text)
open_new_bash_file.write(to_write)
open_new_bash_file.close()

