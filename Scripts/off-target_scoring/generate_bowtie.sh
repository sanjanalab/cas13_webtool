#!/bin/bash

python generate_bash_scripts_bowtie_runner.py "Human/mRNA/*" "'indices/human'" "'bowtie_output/'"
python launch_bash_scripts.py "'bash_script_run_bowtie/human_mrna*'"


#Commented out for sake of git-lab example

# python generate_bash_scripts_bowtie_runner.py "Human/ncRNA/*" "'indices/human'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/human_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "Mouse/mRNA/*" "'indices/mouse'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/mouse_mrna*'"
# python generate_bash_scripts_bowtie_runner.py "Mouse/ncRNA/*" "'indices/mouse'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/mouse_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "Fish/mRNA/*" "'indices/fish'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/fish_mrna*'"
# python generate_bash_scripts_bowtie_runner.py "Fish/ncRNA/*" "'indices/fish'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/fish_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "Fly/mRNA/*" "'indices/fly'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/fly_mrna*'"
# python generate_bash_scripts_bowtie_runner.py "Fly/ncRNA/*" "'indices/fly'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/fly_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "Plant/mRNA/*" "'indices/plant'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/plant_mrna*'"
# python generate_bash_scripts_bowtie_runner.py "Plant/ncRNA/*" "'indices/plant'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/plant_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "Worm/mRNA/*" "'indices/worm'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/worm_mrna*'"
# python generate_bash_scripts_bowtie_runner.py "Worm/ncRNA/*" "'indices/worm'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/worm_ncrna*'"

# python generate_bash_scripts_bowtie_runner.py "SARS/second_level/*" "'indices/human'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/sars*'"

# python generate_bash_scripts_bowtie_runner.py "MERS/second_level/*" "'indices/human'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/mers*'"

# python generate_bash_scripts_bowtie_runner.py "HIV1/second_level/*" "'indices/human'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/hiv1*'"

# python generate_bash_scripts_bowtie_runner.py "H1N1/second_level/*" "'indices/human'" "'bowtie_output/'"
# python launch_bash_scripts.py "'bash_script_run_bowtie/h1n1*'"




