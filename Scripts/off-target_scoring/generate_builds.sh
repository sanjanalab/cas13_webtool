#!/bin/bash

wget 'ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.pc_transcripts.fa.gz'
wget 'ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.lncRNA_transcripts.fa.gz'
cat gencode.v19.pc_transcripts.fa gencode.v19.lncRNA_transcripts.fa > human.fa
mv human.fa  builds/


# Commented out for sake of running git-lab example 

# wget 'ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M24/gencode.vM24.pc_transcripts.fa.gz'
# wget 'ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M24/gencode.vM24.lncRNA_transcripts.fa.gz'
# cat gencode.vM24.pc_transcripts.fa gencode.vM24.lncRNA_transcripts.fa > mouse.fa
# mv mouse.fa  builds/

# wget 'ftp://ftp.ensembl.org/pub/release-103/fasta/danio_rerio/cdna/Danio_rerio.GRCz11.cdna.all.fa.gz'
# wget 'ftp://ftp.ensembl.org/pub/release-103/fasta/danio_rerio/ncrna/Danio_rerio.GRCz11.ncrna.fa.gz'
# cat Danio_rerio.GRCz11.cdna.all.fa Danio_rerio.GRCz11.ncrna.fa > fish.fa
# mv fish.fa  builds/

# wget 'ftp://ftp.ensemblgenomes.org/pub/metazoa/release-50/fasta/caenorhabditis_elegans/cdna/Caenorhabditis_elegans.WBcel235.cdna.all.fa.gz'
# wget 'ftp://ftp.ensemblgenomes.org/pub/metazoa/release-50/fasta/caenorhabditis_elegans/ncrna/Caenorhabditis_elegans.WBcel235.ncrna.fa.gz'
# cat Caenorhabditis_elegans.WBcel235.cdna.all.fa  Caenorhabditis_elegans.WBcel235.ncrna.fa > worm.fa
# mv worm.fa  builds/

# wget 'ftp://ftp.ensemblgenomes.org/pub/metazoa/release-50/fasta/drosophila_melanogaster/cdna/Drosophila_melanogaster.BDGP6.32.cdna.all.fa.gz'
# wget 'ftp://ftp.ensemblgenomes.org/pub/metazoa/release-50/fasta/drosophila_melanogaster/ncrna/Drosophila_melanogaster.BDGP6.32.ncrna.fa.gz'
# cat Drosophila_melanogaster.BDGP6.cdna.all.fa Drosophila_melanogaster.BDGP6.ncrna.fa > fly.fa
# mv fly.fa  builds/

# wget 'ftp://ftp.ensemblgenomes.org/pub/plants/release-50/fasta/arabidopsis_thaliana/cdna/Arabidopsis_thaliana.TAIR10.cdna.all.fa.gz'
# wget 'ftp://ftp.ensemblgenomes.org/pub/plants/release-50/fasta/arabidopsis_thaliana/ncrna/Arabidopsis_thaliana.TAIR10.ncrna.fa.gz'
# cat Arabidopsis_thaliana.TAIR10.cdna.all.fa Arabidopsis_thaliana.TAIR10.ncrna.fa > plant.fa
# mv plant.fa  builds/