#!/bin/bash

python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Human/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/human.txt'" 
sbatch bash_script_generating_fastas/human_mrna.sh


# Commented out for sake of running git-lab example 

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Human/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/human.txt'" 
# sbatch bash_script_generating_fastas/human_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Mouse/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/mouse.txt'" 
# sbatch bash_script_generating_fastas/mouse_mrna.sh
# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Mouse/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/mouse.txt'"
# sbatch bash_script_generating_fastas/mouse_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Worm/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/worm.txt'" 
# sbatch bash_script_generating_fastas/worm_mrna.sh
# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Worm/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/worm.txt'"
# sbatch bash_script_generating_fastas/worm_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Fly/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/fly.txt'"
# sbatch bash_script_generating_fastas/fly_mrna.sh
# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Fly/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/fly.txt'"
# sbatch bash_script_generating_fastas/fly_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Fish/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/fish.txt'"
# sbatch bash_script_generating_fastas/fish_mrna.sh
# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Fish/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/fish.txt'"
# sbatch bash_script_generating_fastas/fish_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Plant/mRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/plant.txt'"
# sbatch bash_script_generating_fastas/plant_mrna.sh
# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/Plant/ncRNA/*'" "'fastas/'" "'formatted_gene_lists_expanded/plant.txt'"
# sbatch bash_script_generating_fastas/plant_ncrna.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/SARS/second_level/*'" "'fastas/'" "'SARS'" 
# sbatch bash_script_generating_fastas/sars_second_level.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/MERS/second_level/*'" "'fastas/'" "'MERS'"
# sbatch bash_script_generating_fastas/mers_second_level.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/H1N1/second_level/*'" "'fastas/'" "'H1N1'"
# sbatch bash_script_generating_fastas/h1n1_second_level.sh

# python  generate_bash_scripts_fasta_generator.py "'PredictedGuides/HIV1/second_level/*'" "'fastas/'" "'HIV1'"
# sbatch bash_script_generating_fastas/hiv1_second_level.sh

