#!/bin/bash
python generate_bash_scripts_final_generator.py "bowtie_output/Human/mRNA/*" "PredictedGuides/Human/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
python launch_bash_scripts.py "'bash_script_generating_final/human_mrna*'"


# Commented out for sake of git-lab example

# python generate_bash_scripts_final_generator.py "bowtie_output/Human/ncRNA/*" "PredictedGuides/Human/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/human_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/Mouse/mRNA/*" "PredictedGuides/Mouse/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/mouse.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/mouse_mrna*'"
# python generate_bash_scripts_final_generator.py "bowtie_output/Mouse/ncRNA/*" "PredictedGuides/Mouse/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/mouse.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/mouse_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/Fish/mRNA/*" "PredictedGuides/Fish/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/fish.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/fish_mrna*'"
# python generate_bash_scripts_final_generator.py "bowtie_output/Fish/ncRNA/*" "PredictedGuides/Fish/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/fish.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/fish_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/Worm/mRNA/*" "PredictedGuides/Worm/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/worm.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/worm_mrna*'"
# python generate_bash_scripts_final_generator.py "bowtie_output/Worm/ncRNA/*" "PredictedGuides/Worm/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/worm.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/worm_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/Plant/mRNA/*" "PredictedGuides/Plant/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/plant.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/plant_mrna*'"
# python generate_bash_scripts_final_generator.py "bowtie_output/Plant/ncRNA/*" "PredictedGuides/Plant/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/plant.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/plant_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/Fly/mRNA/*" "PredictedGuides/Fly/mRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/fly.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/fly_mrna*'"
# python generate_bash_scripts_final_generator.py "bowtie_output/Fly/ncRNA/*" "PredictedGuides/Fly/ncRNA/" "'final_tables/'" "'formatted_gene_lists_expanded/fly.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/fly_ncrna*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/SARS/second_level/*" "'PredictedGuides/SARS/second_level/'" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/sars*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/MERS/second_level/*" "'PredictedGuides/MERS/second_level/'" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/mers*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/HIV1/second_level/*" "'PredictedGuides/HIV1/second_level/'" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/hiv1*'"

# python generate_bash_scripts_final_generator.py "bowtie_output/H1N1/second_level/*" "'PredictedGuides/H1N1/second_level/'" "'final_tables/'" "'formatted_gene_lists_expanded/human.txt'"
# python launch_bash_scripts.py "'bash_script_generating_final/h1n1*'"
