#!/bin/bash

python  generate_bash_scripts_index_builder.py "'builds/human.fa'" "'indices/human'"
sbatch bash_script_build_indices/human.sh


# Commented out for sake of running git-lab example 

# python  generate_bash_scripts_index_builder.py "'builds/mouse.fa'" "'indices/mouse'"
# sbatch bash_script_build_indices/mouse.sh

# python  generate_bash_scripts_index_builder.py "'builds/fish.fa'" "'indices/fish'"
# sbatch bash_script_build_indices/fish.sh

# python  generate_bash_scripts_index_builder.py "'builds/worm.fa'" "'indices/worm'"
# sbatch bash_script_build_indices/worm.sh

# python  generate_bash_scripts_index_builder.py "'builds/fly.fa'" "'indices/fly'"
# sbatch bash_script_build_indices/fly.sh

# python  generate_bash_scripts_index_builder.py "'builds/plant.fa'" "'indices/plant'"
# sbatch bash_script_build_indices/plant.sh
