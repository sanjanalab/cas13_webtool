import os
import glob
import sys
import multiprocessing

def generate_fastas(file, sub_dir_3, conversion_list):
	print("Started: ", file)
	fasta_entries = []
	transcript_id = file.split("_CasRxguides.csv")[0].split("/")[-1]
	open_csv = open(file, "r")
	read_csv = open_csv.read()
	entries_csv = read_csv.splitlines()
	entries_csv.remove(entries_csv[0])
	if conversion_list != "SARS" and conversion_list != "MERS" and conversion_list != "HIV1" and conversion_list != "H1N1":
		open_conversion_list = open(conversion_list, "r")
		read_conversion_list = open_conversion_list.read()
		conversion_list_entries = read_conversion_list.splitlines()
		for j in conversion_list_entries:
			if transcript_id == j.split(",")[1]:
				gene_id = j.split(",")[0]
				break
			else: 
				gene_id = "NONE"
		for j in entries_csv:
			header = j.split(",")[0] + "_" + transcript_id + "_" + gene_id
			fasta_entry= ">%s\n%s" % (header, j.split(",")[1])
			fasta_entries.append(fasta_entry)		
    
	else:                   
		gene_id = file.split("_CasRxguides.csv")[0].split("/")[-1].split(".")[0]
		for j in entries_csv:
			header = j.split(",")[0] + "_" + transcript_id + "_" + gene_id
			fasta_entry= ">%s\n%s" % (header, j.split(",")[1])
			fasta_entries.append(fasta_entry)
	new_fasta_path = sub_dir_3 + transcript_id + "_" + gene_id + ".fa"
	open_new_fasta = open(new_fasta_path, "w+")
	open_new_fasta.write("\n".join(fasta_entries))
	print("Finished: ", file)


def convert_csv_to_fasta(input_folder, output_folder, conversion_list):
	sub_dir_1 = output_folder + input_folder.split("/")[-3] + "/"
	if os.path.isdir(sub_dir_1) == False:
		command = "mkdir -p %s" % (sub_dir_1)
		os.system(command)

	sub_dir_2 = sub_dir_1 + input_folder.split("/")[-2] + "/"
	if os.path.isdir(sub_dir_2) == False:
		command = "mkdir -p %s" % (sub_dir_2)
		os.system(command)

	folders = glob.glob(input_folder)
	processes = []
	for folder in folders:
		sub_dir_3 = sub_dir_2 + folder.split("/")[-1] + "/"
		if os.path.isdir(sub_dir_3) == False:
			command = "mkdir -p %s" % (sub_dir_3)
			os.system(command)

		folder_to_glob = folder + "/*"
		files = glob.glob(folder_to_glob)
		
		for i in files:
			p = multiprocessing.Process(target=generate_fastas, args = [i, sub_dir_3, conversion_list])
			p.start()
			processes.append(p)

	for process in processes:
		process.join()


def build_index(fasta_path, output_dir):
 	#load bowtie/1.1.2
 	new_name = output_dir + "/"+ fasta_path.split("/")[-1].split(".fa")[0]
 	command = "bowtie-build %s %s" % (fasta_path, new_name)
 	os.system(command)

def run_bowtie(folder_of_fastas, index, output_dir):
	sub_dir_1 = output_dir + folder_of_fastas.split("/")[-3].strip() + "/"
	if os.path.isdir(sub_dir_1) == False:
		command = "mkdir -p %s" % (sub_dir_1)
		os.system(command)

	sub_dir_2 = sub_dir_1 + folder_of_fastas.split("/")[-2].strip() + "/"
	if os.path.isdir(sub_dir_2) == False:
		command = "mkdir -p %s" % (sub_dir_2)
		os.system(command)
	sub_dir_3 = sub_dir_2 + folder_of_fastas.split("/")[-1].strip() + "/"
	if os.path.isdir(sub_dir_3) == False:
		command = "mkdir -p %s" % (sub_dir_3)
		os.system(command)
	to_glob =  folder_of_fastas + "/*"
	fastas = glob.glob(to_glob)
	for i in fastas:
		output_file = sub_dir_3 + i.split("/")[-1].split(".fa")[0] + ".txt"
		unaligned_file = sub_dir_3 + i.split("/")[-1].split(".fa")[0] + "_unaligned.fa"
		command = "bowtie --nofw  -a --threads 20 -n 3  -f %s %s -S --sam-nohead %s --un %s" % (index, i, output_file, unaligned_file)
		os.system(command)


def generate_expanded_gene_transcript_list(build_path, new_list_path):
	open_build = open(build_path, "r")
	read_build = open_build.read()
	entries = read_build.split(">")
	entries.remove(entries[0])
	new_entries = []
	if "human" in build_path:
		for i in entries:
			transcript_id = i.split("|")[0]
			gene_id = i.split("|")[1].split(".")[0]
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	if "mouse" in build_path:
		for i in entries:
			transcript_id = i.split("|")[0]
			gene_id = i.split("|")[1].split(".")[0]
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	if "fish" in build_path:
		for i in entries:
			transcript_id = i.split("cdna")[0].strip().strip().split("ncrna")[0].strip()
			gene_id = i.split("gene:")[-1].split("gene_biotype:")[0].strip().split(".")[0]		
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	if "worm" in build_path:
		for i in entries: 
			transcript_id = i.split("cdna")[0].strip().split("ncrna")[0].strip()
			gene_id = i.split("gene_biotype:")[0].strip().split("gene:")[-1]
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	if "fly" in build_path:
		for i in entries: 
			transcript_id = i.split("cdna")[0].strip().split("ncrna")[0].strip()
			gene_id = i.split("gene_biotype:")[0].strip().split("gene:")[-1]		
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	if "plant" in build_path:
		for i in entries: 
			transcript_id = i.split("cdna")[0].strip().split("ncrna")[0].strip()
			gene_id = i.split("gene_biotype:")[0].strip().split("gene:")[-1]		
			new_entry = gene_id + "," + transcript_id
			new_entries.append(new_entry)

	new_entries = list(set(new_entries))
	open_new_file = open(new_list_path, "w+")
	open_new_file.write("\n".join(new_entries))

def generate_transcript_gene_dictionary(list_path):
	open_file = open(list_path, "r")
	read_file = open_file.read()
	entries = read_file.splitlines()
	transcript_gene_dictionary = {}
	for i in entries:
		transcript_gene_dictionary[i.split(",")[1]] = i.split(",")[0]
	return transcript_gene_dictionary

def count_off_targets(csv, sam_files, transcript_gene_dictionary, bowtie_files,  csv_files, out_dir):
	print("Began: ", csv)
	unique_guide_rnas_count_dictionary = {}
	transcript_id = csv.split("/")[-1].split("_CasRxguides.csv")[0]
	open_csv = open(csv, "r")
	read_csv = open_csv.read()
	entries_csv = read_csv.splitlines()
	if entries_csv[0] == "GuideName":
		entries_csv.remove(entries_csv[0])
	gene_id = ""
	if sam_files.split("/")[-3] != "SARS" and sam_files.split("/")[-3] != "MERS" and sam_files.split("/")[-3] != "HIV1" and sam_files.split("/")[-3] != "H1N1":
		for j in entries_csv:
			grna_name = j.split(",")[0]
			gene_id = transcript_gene_dictionary[transcript_id]
			header_entry = grna_name + "_" + transcript_id + "_" + gene_id
			unique_guide_rnas_count_dictionary[header_entry] = [[],[],[],[]]

	else:			
		gene_id = csv.split("/")[-1].split("_CasRxguides.csv")[0].split(".")[0]
		for j in entries_csv:
			grna_name = j.split(",")[0]
			header_entry = grna_name + "_" + transcript_id + "_" + gene_id
			unique_guide_rnas_count_dictionary[header_entry] = [[],[],[],[]]

	print("counts dictionary made (for each grna) for", csv)
	sam_file = ""
	for i in bowtie_files:
		if sam_files.split("/")[-3] != "MERS" and sam_files.split("/")[-3] != "HIV1" and sam_files.split("/")[-3] != "H1N1":
			if transcript_id == i.split("/")[-1].split("_")[0]:
				sam_file = i
				break
		else: 
			prefix = i.split("/")[-1].split(".txt")[0]
			for_comp = prefix.replace(transcript_id, "")
			transcript_id_for_comp = "_" + gene_id 
			if transcript_id_for_comp == for_comp:
				sam_file = i
				break

	print(sam_file, csv)
	# record how many times each guide maps against a unique reference sequence at a different threshold, condense to only unique hits
	with open(sam_file) as sam_file_entries:
		for line in sam_file_entries:
			if "NM:i:0" in line: 
				if transcript_gene_dictionary.get(line.split("\t")[0].split("_")[1], "Not Found") != transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()]:
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][0].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])

					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][0] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][0]))
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1]))
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))		
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))
			elif "NM:i:1" in line:
				if transcript_gene_dictionary.get(line.split("\t")[0].split("_")[1], "Not Found") != transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()]:
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])

					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][1]))
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))	
			elif "NM:i:2" in line:
				if transcript_gene_dictionary.get(line.split("\t")[0].split("_")[1], "Not Found") != transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()]:
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))		
			elif "NM:i:3" in line:
				if transcript_gene_dictionary.get(line.split("\t")[0].split("_")[1], "Not Found") != transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()]:
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3].append(transcript_gene_dictionary[line.split("\t")[2].split("|")[0].strip()])
					
					unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][3] = list(set(unique_guide_rnas_count_dictionary[line.split("\t")[0].strip()][2]))
	print('All Mapped Genes Entered Into Dictionary for', csv)

# re-generate dictionary if its a virus
	if sam_file.split("/")[-4] == "SARS" or sam_file.split("/")[-4] == "MERS" or sam_file.split("/")[-4] == "HIV1" or sam_file.split("/")[-4] == "H1N1":
		transcript_gene_dictionary = {}
		for j in csv_files:
			transcript_gene_dictionary[csv.split("/")[-1].split("_CasRxguides.csv")[0]] = csv.split("/")[-1].split("_CasRxguides.csv")[0].split(".")[0]
		print('Virus gene list remade')

	#generate new csv files
	open_old_csv = open(csv, "r")
	read_old_csv = open_old_csv.read()
	old_csv_entries = read_old_csv.splitlines()
	old_csv_entries.remove(old_csv_entries[0])

	new_csv_path = out_dir + csv.split("/")[-1]
	open_new_csv = open(new_csv_path, "w+")
	open_new_csv.write("GuideName,GuideSeq,MatchPos,GuideScores,Rank,standardizedGuideScores,quartiles,mapped0e:mapped1e:mapped2e:mapped3e\n")
	gene = ""
	if sam_file.split("/")[-4] == "SARS" or sam_file.split("/")[-4] == "MERS" or sam_file.split("/")[-4] == "HIV1" or sam_file.split("/")[-4] == "H1N1":
		gene = csv.split("/")[-1].split("_CasRxguides.csv")[0].split(".")[0]
	else:
		gene = transcript_gene_dictionary[csv.split("/")[-1].split("_CasRxguides.csv")[0]]

	for j in old_csv_entries:
		dictionary_key = j.split(",")[0] + "_" + csv.split("/")[-1].split("_CasRxguides.csv")[0] + "_" + gene
		updated_entry = j + "," + str(len(unique_guide_rnas_count_dictionary[dictionary_key][0])) + ":" + str(len(unique_guide_rnas_count_dictionary[dictionary_key][1])) + ":" + str(len(unique_guide_rnas_count_dictionary[dictionary_key][2])) + ":" + str(len(unique_guide_rnas_count_dictionary[dictionary_key][2]))
		open_new_csv.write(updated_entry + "\n")
	print("Finished: ", csv)

def generate_final_csvs(sam_files, csvs, out_dir, gene_transcript_list_path):
	sub_dir_1 = out_dir + sam_files.split("/")[-3].strip() + "/"
	if os.path.isdir(sub_dir_1) == False:
		command = "mkdir -p %s" % (sub_dir_1)
		os.system(command)

	sub_dir_2 = sub_dir_1 + sam_files.split("/")[-2].strip() + "/"
	if os.path.isdir(sub_dir_2) == False:
		command = "mkdir -p %s" % (sub_dir_2)
		os.system(command)

	sub_dir_3 = sub_dir_2 + sam_files.split("/")[-1].strip() + "/"
	if os.path.isdir(sub_dir_3) == False:
		command = "mkdir -p %s" % (sub_dir_3)
		os.system(command)
	to_glob = csvs + "/*"
	csv_files = glob.glob(to_glob)

	to_glob = sam_files + "/*.txt"
	bowtie_files = glob.glob(to_glob) 
	transcript_gene_dictionary = generate_transcript_gene_dictionary(gene_transcript_list_path)
	
	#make a dictionary to contain counts corresponding to the number of times each guide mapped against a ref seq at varying thresholds
	processes = []
	for i in csv_files:
		p = multiprocessing.Process(target=count_off_targets, args = [i,  sam_files, transcript_gene_dictionary, bowtie_files, csv_files, sub_dir_3])
		p.start()
		processes.append(p)

	for process in processes:
		process.join()
	print("Done Making New CSVs")

